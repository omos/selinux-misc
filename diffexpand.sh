#!/bin/bash

set -e
set -o pipefail

n="$1"; shift
policy="$1"; shift

tmpdir="$(mktemp -d)"

trap 'rm -rf "$tmpdir"' EXIT

checkpolicy -o "$tmpdir/policy1.cil" -bMC "$policy"
secilc -X "$n" -M 1 -f /dev/null -o "$tmpdir/policy-expanded" "$tmpdir/policy1.cil"
checkpolicy -o "$tmpdir/policy2.cil" -bMC "$tmpdir/policy-expanded"

git diff --no-index "$tmpdir/policy1.cil" "$tmpdir/policy2.cil"
