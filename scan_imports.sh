#!/bin/bash

set -e
set -o pipefail

RELEASE="32"
PACKAGE="libselinux"
SYMBOLS="dir_xattr_list|map_class|map_decision|map_perm|myprtinf_compat|unmap_class|unmap_perm"

tmpdir="$(mktemp -d)"

trap 'rm -rf $tmpdir' EXIT

cd "$tmpdir"

dnf repoquery --releasever "$RELEASE" --whatrequires "$PACKAGE" >packages.list

dnf download --releasever "$RELEASE" $(cat packages.list)

mkdir root

for rpm in ./*.rpm; do
	rpm2cpio "$rpm" 2>/dev/null | cpio -idmv -D root &>/dev/null
done

find root -type f -exec objdump -T \{\} \; 2>/dev/null | grep -E "$SYMBOLS"
