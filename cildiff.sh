#!/bin/bash

set -e
set -o pipefail

policy1="$1"; shift
policy2="$1"; shift

tmpdir="$(mktemp -d)"

trap 'rm -rf "$tmpdir"' EXIT

checkpolicy -o "$tmpdir/policy1.cil" -bMC "$policy1"
checkpolicy -o "$tmpdir/policy2.cil" -bMC "$policy2"

ls -l "$tmpdir"

git diff --no-index "$tmpdir/policy1.cil" "$tmpdir/policy2.cil"
