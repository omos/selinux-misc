#!/bin/bash

set -e
set -o pipefail

function any_line()
{
	local n=$(wc -l)
	[ $n -gt 1 ] && echo "$n"
	[ $n -ne 0 ]
}

function emit_cil_base()
{
cat <<EOF
(handleunknown deny)
(mls true)

(sid unlabeled)
(sidcontext unlabeled (u01 r01 t01 ((s01) (s01))))
(sid security)
(sidcontext security (u01 r01 t01 ((s01) (s01))))
(sid kernel)
(sidcontext kernel (u01 r01 t01 ((s01) (s01))))
(sidorder (kernel security unlabeled ))

(class c01 (p01 p02 p03 p04 p05))
(class c02 (p01 p02 p03 p04 p05))
(class c03 (p01 p02 p03 p04 p05))
(class c04 (p01 p02 p03 p04 p05))
(class cx (ioctl))
(classorder (c01 c02 c03 c04 cx))

(role r01)
(roletype r01 t01)

(type t01)
(type t02)
(type t03)
(type t04)
(type t05)
(type t06)
(type t07)
(type t08)

(typeattribute a01)
(typeattribute a02)
(typeattribute a03)
(typeattribute a04)
(typeattribute a05)
(typeattribute a06)
(typeattribute a07)
(typeattribute a08)

(typeattributeset a01 (t01 t02))
(typeattributeset a02 (t01 t02))
(typeattributeset a03 (t01 t02 t03 t04))
(typeattributeset a04 (t03 t04))
(typeattributeset a05 (t03 t04 t05))
(typeattributeset a06 (t04 t05))

(user u01)
(userrole u01 r01)
(userlevel u01 (s01))
(userrange u01 ((s01) (s01)))

(boolean b01 false)
(boolean b02 true)

(sensitivity s01)
(sensitivityorder (s01))
EOF
}

function emit_cil_av_rule()
{
	local kind="$1"; shift
	local stype="$1"; shift
	local ttype="$1"; shift
	local class="$1"; shift
	local perms="$1"; shift

	echo "($kind $stype $ttype ($class ($perms)))"
}

function emit_cil_xperm_rule()
{
	local kind="$1"; shift
	local stype="$1"; shift
	local ttype="$1"; shift
	local class="$1"; shift
	local perm="$1"; shift
	local xfrom="$1"; shift
	local xto="$1"; shift

	echo "(${kind}x $stype $ttype ($perm $class (range $xfrom $xto)))"
}

function expand_attribute()
{
	if seinfo --flat -a "$1" "$POLICY" | any_line >/dev/null; then
		seinfo --flat -xa "$1" "$POLICY" | tail -n +2 | tr -d '\t'
	else
		echo "$1"
	fi
}

function search_av_rule()
{
	echo "  Checking rule '$(emit_cil_av_rule "$@")'..."

	local kind="$1"; shift
	local stype="$1"; shift
	local ttype="$1"; shift
	local class="$1"; shift
	local perms="$1"; shift

	for stype in $(expand_attribute "$stype"); do
		for ttype in $(expand_attribute "$ttype"); do
			for perm in $perms; do
				sesearch "--$kind" \
					-s "$stype" -t "$ttype" \
					-c "$class" -p "$perm" \
					"$POLICY" | any_line
			done
		done
	done
}

function search_xperm_rule()
{
	echo "  Checking rule '$(emit_cil_xperm_rule "$@")'..."

	local kind="$1"; shift
	local stype="$1"; shift
	local ttype="$1"; shift
	local class="$1"; shift
	local perm="$1"; shift
	local xfrom="$1"; shift
	local xto="$1"; shift

	for stype in $(expand_attribute "$stype"); do
		for ttype in $(expand_attribute "$ttype"); do
			# check only bounding values to save time...
			for xperm in $xfrom $xto; do
				sesearch "--${kind}xperm" \
					-s "$stype" -t "$ttype" \
					-c "$class" -p "$perm" -x "$xperm" \
					"$POLICY" | any_line
			done
		done
	done
}

function apply_av_rules()
{
	for kind in "allow" "dontaudit" "auditallow"; do
		$1 "$kind" "a01" "t06" "c01" "p01 p02" # \ just one of these
		$1 "$kind" "a02" "t06" "c01" "p01 p02" # / is redundant
		$1 "$kind" "t01" "t06" "c01" "p01 p02" # redundant
		$1 "$kind" "t02" "t06" "c01" "p01 p02" # redundant
		$1 "$kind" "t02" "a06" "c01" "p01 p02"

		$1 "$kind" "a04" "t06" "c02" "p01 p02"
		$1 "$kind" "a06" "t06" "c02" "p03 p04 p05"
		$1 "$kind" "a06" "t06" "c02" "p03 p04" # redundant
		$1 "$kind" "t04" "t06" "c02" "p01 p02 p03 p04" # redundant
	done
}

function apply_xperm_rules()
{
	for kind in "allow" "dontaudit" "auditallow"; do
		$1 "$kind" "a01" "t06" "cx" "ioctl" "0x8800" "0x88FF" # \ just one of these
		$1 "$kind" "a02" "t06" "cx" "ioctl" "0x8800" "0x88FF" # / is redundant
		$1 "$kind" "t01" "t06" "cx" "ioctl" "0x8800" "0x88FF" # redundant
		$1 "$kind" "t02" "t06" "cx" "ioctl" "0x8800" "0x88FF" # redundant
		$1 "$kind" "t02" "t06" "cx" "ioctl" "0x8801" "0x8802" # redundant

		$1 "$kind" "t02" "t07" "cx" "ioctl" "0x8800" "0x99FF"
		$1 "$kind" "t02" "t07" "cx" "ioctl" "0x9100" "0x93FF" # redundant
		$1 "$kind" "t02" "t07" "cx" "ioctl" "0x9100" "0x91FF" # redundant
		$1 "$kind" "t02" "t07" "cx" "ioctl" "0x9105" "0x9106" # redundant
	done
}

function emit_cil()
{
	emit_cil_base
	apply_av_rules emit_cil_av_rule
	apply_xperm_rules emit_cil_xperm_rule
}

function search_policy()
{
	apply_av_rules search_av_rule
	apply_xperm_rules search_xperm_rule
}

tmpdir="$(mktemp -d)"

trap 'rm -rf "$tmpdir"' EXIT

echo "Generating CIL..."
emit_cil | tee "$tmpdir/policy.cil"
echo "Compiling policy..."
secilc -c 31 -O -N -M 1 -f /dev/null -o "$tmpdir/policy.kern" "$tmpdir/policy.cil"

echo "Dumping rules..."
for suffix in "" "x"; do
	for kind in "allow" "dontaudit" "auditallow"; do
		checkpolicy -b -C -M -o /dev/stdout "$tmpdir/policy.kern" | \
			grep "($kind$suffix "
	done
done

echo "Checking policy..."
POLICY="$tmpdir/policy.kern" search_policy
