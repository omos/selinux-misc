#!/bin/bash

set -e
set -o pipefail

tmpdir="$(mktemp -d)"

trap 'rm -rf "$tmpdir"' EXIT

checkpolicy -o "$tmpdir/policy.cil" -bMC /dev/stdin
secilc "$@" -f /dev/null -o /dev/stdout "$tmpdir/policy.cil"
